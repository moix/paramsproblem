import { useEffect, useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import { useLoaderData } from "react-router-dom";

const Page = () => {
  const [count, setCount] = useState([""]);
  const [url, setUrl] = useState([""]);
  const data = useLoaderData();
  console.log("count", count);
  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <p>url</p>
      <input
        value={url}
        style={{
          width: "50%",
          height: "50px",
        }}
        onChange={(e) => {
          let val = e.target.value.split("?");
          console.log(val);
          if (val.length >= 1) {
            setUrl(e.target.value);
            val.shift();
            let params = val.toString().split("&");
            console.log("url===>>", params);
            setCount([...params]);
          }
          setUrl(e.target.value);
        }}
      />
      <p>Params</p>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          gap: "20px",
        }}
      >
        {count.length !== 0 &&
          count.map((item, index) => (
            <div
              key={index}
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <div style={{ display: "flex", gap: "4px" }}>
                <p>Params</p>
                <input
                  style={{
                    width: "100%",
                    height: "50px",
                  }}
                  value={item?.split("=")[0]}
                  onChange={(e) => {
                    let temp = url.split("?");
                    temp.shift();
                    console.log("par,,,,", e.target.value);
                    console.log("params--->", temp);
                    let params = temp.toString().split("&");
                    let val = params[index].split("=");
                    val[0] = e.target.value;
                    console.log("val===>", val);
                    params[index] = val.join("=");
                    console.log("====>params", params);
                    setUrl(url.split("?")[0] + "?" + params.join("&"));
                    let cnt = [...count];
                    let te = cnt[index].split("=");
                    te[0] = e.target.value;
                    cnt[index] = te.join("=");
                    setCount(cnt);
                  }}
                />
              </div>
              <div style={{ display: "flex", gap: "4px" }}>
                <p>Value</p>
                <input
                  style={{
                    width: "100%",
                    height: "50px",
                  }}
                  value={item !== "" ? item?.split("=")[1] : ""}
                  onChange={(e) => {
                    let temp = url.split("?");
                    temp.shift();
                    console.log("par,,,,", e.target.value);
                    console.log("params--->", temp);
                    let params = temp.toString().split("&");
                    let val = params[index].split("=");
                    val[1] = e.target.value;
                    console.log("val===>", val);
                    params[index] = val.join("=");
                    console.log("====>params", params);
                    setUrl(url.split("?")[0] + "?" + params.join("&"));
                    let cnt = [...count];
                    let te = cnt[index].split("=");
                    te[1] = e.target.value;
                    cnt[index] = te.join("=");
                    setCount(cnt);
                  }}
                />
              </div>
            </div>
          ))}
      </div>

      <div className="grid-container">
        {data.products.map((item, key) => (
          <div key={key} className="card grid-item">
            <img src={item.images[0]} />
            <h1>{item.title}</h1>
            <p>{item.description.substring(0, 20)}</p>
          </div>
        ))}
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </>
  );
};

export default Page;
