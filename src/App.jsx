import { useState } from "react";
import "./App.css";
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Link,
} from "react-router-dom";
import Page from "./page";

function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Page />,
      loader: async () => {
        return fetch(`https://dummyjson.com/products`);
      },
      errorElement: <div>Some Error Occured</div>,
    },
    {
      path: "about",
      element: <div>About</div>,
    },
  ]);

  return <RouterProvider router={router} />;
}

export default App;
